
/*  package com.example.demo.config;
  
  import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
  
  @Configuration
  @EnableWebSecurity 
  public class SecurityConfig extends WebSecurityConfigurerAdapter {
  
  @Autowired 
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  
  @Autowired 
  private DataSource dataSource;
  
  @Value("${spring.queries.users-query}") 
  private String usersQuery;
  
  @Value("${spring.queries.roles-query}") 
  private String rolesQuery;
  
  
  
  @Bean 
  public BCryptPasswordEncoder passwordEncoder() {
  
  BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
  
  return bCryptPasswordEncoder; 
  }
  
  @Override protected void configure(AuthenticationManagerBuilder auth) throws Exception { 
	  
	  auth
	  
	  .jdbcAuthentication()
	  .usersByUsernameQuery(usersQuery)
	  .authoritiesByUsernameQuery(rolesQuery) 
	  .dataSource(dataSource)
	  .passwordEncoder(bCryptPasswordEncoder); 
	  
  }
  
  
  @Override 
  protected void configure(HttpSecurity http) throws Exception {
  
	  			http.csrf().disable(); 
	  			
	  			http.authorizeRequests()
	  			.antMatchers(
	  		           HttpMethod.GET,
	  		           "/index*", "/static/**", "/.js", "/.json", "/*.ico")
	  		           .permitAll()
	  			    .antMatchers("/")
	  				.permitAll()
	  				.antMatchers("/login").permitAll()
	  				.antMatchers("/admin/**").
	  				 hasAuthority("ADMIN").anyRequest()
	  				.authenticated() .and()
	  				.csrf().disable()
	  				.formLogin() .loginPage("/login")
	  				.defaultSuccessUrl("/homePage",true) 
	  				.and().logout(); 
	  			
  }
  
  
  }
 */