package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Js_Notification")
public class JsNotification {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "notf_id")
	private Long notfId;
	
	@Column(name = "employer")
	private String employer;
	
	@Column(name = "employer_location")
	private String employerLocation;
	
	@Column(name = "start_date_time")
	private String startDateAndTime;
	
	@Column(name = "end_date_tine")
	private String endDateAndTime;
	
	@Column(name = "amount_to_be_paid")
	private String amountToBePaid; 
	
	@Column(name = "Status")
	private Long status;
	
	@Column(name="last_modified_date")
	private Date lastModifiedDate;

	@Column(name="last_modified_by")
	private String lastModifiedBy;
	
	@OneToOne
	@JoinColumn(name="job_seeker",referencedColumnName = "js_id")
	private UserJobSeeker userJobSeeker;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id")
	private PostNewJobDetails jobNotification;
	

	public Long getNotfId() {
		return notfId;
	}

	public void setNotfId(Long notfId) {
		this.notfId = notfId;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getEmployerLocation() {
		return employerLocation;
	}

	public void setEmployerLocation(String employerLocation) {
		this.employerLocation = employerLocation;
	}

	public String getStartDateAndTime() {
		return startDateAndTime;
	}

	public void setStartDateAndTime(String startDateAndTime) {
		this.startDateAndTime = startDateAndTime;
	}

	public String getEndDateAndTime() {
		return endDateAndTime;
	}

	public void setEndDateAndTime(String endDateAndTime) {
		this.endDateAndTime = endDateAndTime;
	}

	public String getAmountToBePaid() {
		return amountToBePaid;
	}

	public void setAmountToBePaid(String amountToBePaid) {
		this.amountToBePaid = amountToBePaid;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public UserJobSeeker getUserJobSeeker() {
		return userJobSeeker;
	}

	public void setUserJobSeeker(UserJobSeeker userJobSeeker) {
		this.userJobSeeker = userJobSeeker;
	}

	public PostNewJobDetails getJobNotification() {
		return jobNotification;
	}

	public void setJobNotification(PostNewJobDetails jobNotification) {
		this.jobNotification = jobNotification;
	}

}
