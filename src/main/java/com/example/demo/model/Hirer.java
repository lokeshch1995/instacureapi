package com.example.demo.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="hirer")
public class Hirer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hr_id")
	private Long hrId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "name_of_organization")
	private String nameOfOrganization;
	
	@Column(name = "sector")
	private String sector;
	
	@Column(name = "departments")
	private String departments;
	
	@Column(name = "branches")
	private Long branches;
	
	@Column(name="beds")
	private Long beds;
	
	@Column(name = "head_office_addr")
	private String headOfficeAddr;
	
	@Column(name = "documents")
	private byte[] documents;
	
	@Column(name = "certificates")
	private byte[] certificates;
	
	@Column(name = "prime_poc_name")
	private String primePocName;
	
	@Column(name = "p_designation")
	private String pPocDesignation;
	
	@Column(name = "p_phone_number")
	private String primePocPhone;
	
	@Column(name = "p_office_email")
	private String pPocOfficeEmail;
	
	@Column(name = "status")
	private String status;
	
	@Column(name="last_modified_date")
	private Date lastModifiedDate;
	
	@Column(name="last_modified_by")
	private String lastModifiedBy;
	
	@Column(name = "password")
	private String password;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "hr_id", referencedColumnName = "hr_id")
    private List<PointOfContact> contacts;
	

	public List<PointOfContact> getContacts() {
		return contacts;
	}

	public void setContacts(List<PointOfContact> contacts) {
		this.contacts = contacts;
	}

	public Long getHrId() {
		return hrId;
	}

	public void setHrId(Long hrId) {
		this.hrId = hrId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNameOfOrganization() {
		return nameOfOrganization;
	}

	public void setNameOfOrganization(String nameOfOrganization) {
		this.nameOfOrganization = nameOfOrganization;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getDepartments() {
		return departments;
	}

	public void setDepartments(String departments) {
		this.departments = departments;
	}

	public Long getBranches() {
		return branches;
	}

	public void setBranches(Long branches) {
		this.branches = branches;
	}

	public Long getBeds() {
		return beds;
	}

	public void setBeds(Long beds) {
		this.beds = beds;
	}

	public String getHeadOfficeAddr() {
		return headOfficeAddr;
	}

	public void setHeadOfficeAddr(String headOfficeAddr) {
		this.headOfficeAddr = headOfficeAddr;
	}

	public byte[] getDocuments() {
		return documents;
	}

	public void setDocuments(byte[] documents) {
		this.documents = documents;
	}

	public byte[] getCertificates() {
		return certificates;
	}

	public void setCertificates(byte[] certificates) {
		this.certificates = certificates;
	}

	public String getPrimePocName() {
		return primePocName;
	}

	public void setPrimePocName(String primePocName) {
		this.primePocName = primePocName;
	}

	public String getpPocDesignation() {
		return pPocDesignation;
	}

	public void setpPocDesignation(String pPocDesignation) {
		this.pPocDesignation = pPocDesignation;
	}

	public String getPrimePocPhone() {
		return primePocPhone;
	}

	public void setPrimePocPhone(String primePocPhone) {
		this.primePocPhone = primePocPhone;
	}

	public String getpPocOfficeEmail() {
		return pPocOfficeEmail;
	}

	public void setpPocOfficeEmail(String pPocOfficeEmail) {
		this.pPocOfficeEmail = pPocOfficeEmail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
