package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Jobseeker_Ratings")
public class JobSeekerRatings {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "js_rating_id")
	private Long jsRatingId;
	
	@Column(name = "star_count")
	private Long starCount;
	
	@Column(name = "status")
	private String status;
	
	@OneToOne
	@JoinColumn(name = "hr_id",referencedColumnName = "hr_id")
	private Hirer hirer;
	
	@OneToOne
	@JoinColumn(name="js_id",referencedColumnName = "js_id")
	private UserJobSeeker userJobSeeker;

	@Column(name="last_modified_date")
	private Date lastModifiedDate;
	
	@Column(name="last_modified_by")
	private String lastModifiedBy;

	public Long getJsRatingId() {
		return jsRatingId;
	}

	public void setJsRatingId(Long jsRatingId) {
		this.jsRatingId = jsRatingId;
	}

	public Long getStarCount() {
		return starCount;
	}

	public void setStarCount(Long starCount) {
		this.starCount = starCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

	public Hirer getHirer() {
		return hirer;
	}

	public void setHirer(Hirer hirer) {
		this.hirer = hirer;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public UserJobSeeker getUserJobSeeker() {
		return userJobSeeker;
	}

	public void setUserJobSeeker(UserJobSeeker userJobSeeker) {
		this.userJobSeeker = userJobSeeker;
	}
	
}
