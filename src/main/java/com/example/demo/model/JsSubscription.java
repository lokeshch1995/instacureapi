package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Js_Subscription")
public class JsSubscription {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "sub_id")	
private Long subId;

@Column(name = "sub_name")
private String subName;

@Column(name = "price")
private String price;

@Column(name = "status")
private String status;

@Column(name = "duration")
private Long duration;


@Column(name="last_modified_date")
private Date lastModifiedDate;

@Column(name="last_modified_by")
private String lastModifiedBy;

public Long getSubId() {
	return subId;
}

public void setSubId(Long subId) {
	this.subId = subId;
}

public String getSubName() {
	return subName;
}

public void setSubName(String subName) {
	this.subName = subName;
}

public String getPrice() {
	return price;
}

public void setPrice(String price) {
	this.price = price;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public Long getDuration() {
	return duration;
}

public void setDuration(Long duration) {
	this.duration = duration;
}

public Date getLastModifiedDate() {
	return lastModifiedDate;
}

public void setLastModifiedDate(Date lastModifiedDate) {
	this.lastModifiedDate = lastModifiedDate;
}

public String getLastModifiedBy() {
	return lastModifiedBy;
}

public void setLastModifiedBy(String lastModifiedBy) {
	this.lastModifiedBy = lastModifiedBy;
}


}
