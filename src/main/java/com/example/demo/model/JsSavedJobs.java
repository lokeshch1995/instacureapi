package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Js_Saved_jobs")
public class JsSavedJobs {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "js_savedjobs_id")
	private Long jsSavedJobsId;
	
	@OneToOne
	@JoinColumn(name="job_seeker",referencedColumnName = "js_id")
	private UserJobSeeker userJobSeeker;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id")
	private PostNewJobDetails jobsSaved;
	
	@Column(name = "status")
	private String status;
	
	@Column(name="last_modified_date")
	private Date lastModifiedDate;
	
	@Column(name="last_modified_by")
	private String lastModifiedBy;

	public Long getJsSavedJobsId() {
		return jsSavedJobsId;
	}

	public void setJsSavedJobsId(Long jsSavedJobsId) {
		this.jsSavedJobsId = jsSavedJobsId;
	}

	public UserJobSeeker getUserJobSeeker() {
		return userJobSeeker;
	}

	public void setUserJobSeeker(UserJobSeeker userJobSeeker) {
		this.userJobSeeker = userJobSeeker;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public PostNewJobDetails getJobsSaved() {
		return jobsSaved;
	}

	public void setJobsSaved(PostNewJobDetails jobsSaved) {
		this.jobsSaved = jobsSaved;
	}

}
