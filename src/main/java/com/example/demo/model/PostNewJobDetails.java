package com.example.demo.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "post_new_job")
public class PostNewJobDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "post_new_job_id")
	private Long postNewJobId;
	
	@Column(name = "position_title")
	private String positionTitle;
	
	@Column(name = "category")
	private String category;
	
	@Column(name = "assg_start_date")
	private Date assgStartDate;
	
	@Column(name = "assg_planned_enddate")
	private Date assgPlannedEnddate;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "salary_details")
	private String salaryDetails;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "Status")
	private String status;
	
	@Column(name="last_modified_date")
	private Date lastModifiedDate;
	
	@Column(name="last_modified_by")
	private String lastModifiedBy;

	@OneToOne
	@JoinColumn(name = "hr_id",referencedColumnName = "hr_id")
	private Hirer hrId;
	
	@OneToMany(mappedBy = "jobsSaved")
	private List<JsSavedJobs> savedJobs;
	
	@OneToMany(mappedBy = "previousJobs")
	private List<EmploymentHistory> previousJobs;
	
	@OneToMany(mappedBy = "jobNotification")
	private List<JsNotification> jobNotification;

	public Hirer getHrId() {
		return hrId;
	}

	public void setHrId(Hirer hrId) {
		this.hrId = hrId;
	}

	public Long getPostNewJobId() {
		return postNewJobId;
	}

	public void setPostNewJobId(Long postNewJobId) {
		this.postNewJobId = postNewJobId;
	}

	public String getPositionTitle() {
		return positionTitle;
	}

	public void setPositionTitle(String positionTitle) {
		this.positionTitle = positionTitle;
	}

	public Date getAssgStartDate() {
		return assgStartDate;
	}

	public void setAssgStartDate(Date assgStartDate) {
		this.assgStartDate = assgStartDate;
	}

	public Date getAssgPlannedEnddate() {
		return assgPlannedEnddate;
	}

	public void setAssgPlannedEnddate(Date assgPlannedEnddate) {
		this.assgPlannedEnddate = assgPlannedEnddate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSalaryDetails() {
		return salaryDetails;
	}

	public void setSalaryDetails(String salaryDetails) {
		this.salaryDetails = salaryDetails;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
}
