package com.example.demo.model;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.geo.Point;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"ratings","matchedJobSeekers","roles"})
@Entity
@Table(name = "User_job_seeker")
public class UserJobSeeker {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "js_id")
	private Long jsId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@Column(name = "years_of_exp")
	private Long yearsOfExperience;
	
	@Column(name = "category")
	private Long category;
	
	@Column(name = "specilization")
	private String specilization;
	
	@Column(name="type")
	private String type;
	
	@Column(name = "previous_work_place")
	private String previousWorkPlace;
	
	@Column(name = "place")
	private String place;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "profile_summary")
	private String profileSummary;
	
	@Column(name = "availability_of_insta_hire")
	private boolean switchValue;
	
	@Column(name = "photo")
	private String image;
	
	@Column(name="otp")
	private String otp;
	
	@Column(name = "document")
	private byte[] document ;
	
	@Column(name="last_modified_date")
	private Date lastModifiedDate;
	
	@Column(name="last_modified_by")
	private String lastModifiedBy;
	
	@Column(name="status")
	private String status;
	
	@Column(name = "point", columnDefinition = "POINT")
	private Point point;
	
	@OneToMany(mappedBy = "jsId")
	private List<EmployerRating> ratings;
	
	@OneToMany(mappedBy = "matchedJobSeekers")
	private List<MatchedProfiles> matchedJobSeekers;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "js_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getJsId() {
		return jsId;
	}

	public void setJsId(Long jsId) {
		this.jsId = jsId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getYearsOfExperience() {
		return yearsOfExperience;
	}

	public void setYearsOfExperience(Long yearsOfExperience) {
		this.yearsOfExperience = yearsOfExperience;
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	public String getSpecilization() {
		return specilization;
	}

	public void setSpecilization(String specilization) {
		this.specilization = specilization;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPreviousWorkPlace() {
		return previousWorkPlace;
	}

	public void setPreviousWorkPlace(String previousWorkPlace) {
		this.previousWorkPlace = previousWorkPlace;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProfileSummary() {
		return profileSummary;
	}

	public void setProfileSummary(String profileSummary) {
		this.profileSummary = profileSummary;
	}

	public boolean isSwitchValue() {
		return switchValue;
	}

	public void setSwitchValue(boolean switchValue) {
		this.switchValue = switchValue;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public byte[] getDocument() {
		return document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public List<EmployerRating> getRatings() {
		return ratings;
	}

	public void setRatings(List<EmployerRating> ratings) {
		this.ratings = ratings;
	}

	public List<MatchedProfiles> getMatchedJobSeekers() {
		return matchedJobSeekers;
	}

	public void setMatchedJobSeekers(List<MatchedProfiles> matchedJobSeekers) {
		this.matchedJobSeekers = matchedJobSeekers;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}
	

	
}
