package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employer_rating")
public class EmployerRating {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "js_rating_id")
	private Long employerRatingId;
	
	
	@Column(name = "interaction_with_emp")
	private Long interactionWithEmp ;
	
	@Column(name = "time_management")
	private Long timeManagement ;
	
	@Column(name = "communication")
	private Long communication ;
	
	@Column(name = "experience")
	private Long experience ;
	
	
	@Column(name = "status")
	private String status ;
	
	@Column(name="last_modified_date")
	private Date lastModifiedDate;
	
	@Column(name="last_modified_by")
	private String lastModifiedBy;

	@OneToOne
	@JoinColumn(name = "hirer_id",referencedColumnName = "hr_id")
	private Hirer hirer;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "js_id")
	private UserJobSeeker jsId;
	
	
	public Long getEmployerRatingId() {
		return employerRatingId;
	}

	public void setEmployerRatingId(Long employerRatingId) {
		this.employerRatingId = employerRatingId;
	}


	public Long getInteractionWithEmp() {
		return interactionWithEmp;
	}

	public void setInteractionWithEmp(Long interactionWithEmp) {
		this.interactionWithEmp = interactionWithEmp;
	}

	public Long getTimeManagement() {
		return timeManagement;
	}

	public void setTimeManagement(Long timeManagement) {
		this.timeManagement = timeManagement;
	}

	public Long getCommunication() {
		return communication;
	}

	public void setCommunication(Long communication) {
		this.communication = communication;
	}

	public Long getExperience() {
		return experience;
	}

	public void setExperience(Long experience) {
		this.experience = experience;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	
	
}
