package com.example.demo.model;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;



public class OtpSender {
	
	
	
	// Username that is to be used for submission
	
		private String username;
		// password that is to be used along with username
		String password;
		// Message content that is to be transmitted
		private String message;
		/**
		 * * What type of the message that is to be sent
		 * <ul>
		 * <li>0:means plain * text</li>
		 * <li>1:means flash</li>
		 * <li>2:means Unicode (Message content * should be in Hex)</li>
		 * <li>6:means Unicode Flash (Message content should * be in Hex)</li>
		 * </ul>
		 */
		private String type;
		/**
		 * * Require DLR or not
		 * <ul>
		 * <li>0:means DLR is not Required</li>
		 * <li>1:means * DLR is Required</li>
		 * </ul>
		 */
		private String dlr;
		/**
		 * Destinations to which message is to be sent For submitting more than one
		 * destination at once destinations should be comma separated Like *
		 * 91999000123,91999000124
		 */
		private String destination;
		// Sender Id to be used for submitting the message
		private String source;
		// To what server you need to connect to for submission
		private String server;
		// Port that is to be used like 8080 or 8000
		private int port;

		
		public OtpSender(String server, int port, String username, String password, String message, String dlr, String type,
				String destination, String source) {
			this.username = username;
			this.password = password;
			this.message = message;
			this.dlr = dlr;
			this.type = type;
			this.destination = destination;
			this.source = source;
			this.server = server;
			this.port = port;
		}

		
		public OtpSender() {
			// TODO Auto-generated constructor stub
			super();
		}


		public String submitMessage() 
		{
			HttpURLConnection httpConnection = null;
			String mesage = null;
			try {
				// Url that will be called to submit the message
				URL sendUrl = new URL("http://" + this.server + ":" + this.port + "/bulksms/bulksms");
				HostnameVerifier hostVerfier = new HostnameVerifier() 
				{
					public boolean verify(String urlHostName, SSLSession session) 
					{
						return true;
					}
				};
				trustAllHttpsCertificates();
				httpConnection = (java.net.HttpURLConnection) sendUrl.openConnection();
				// This method sets the method type to POST so that
				// will be send as a POST request 
				httpConnection.setRequestMethod("POST"); 
				// This method is set as true wince we intend to send
				// input to the server 
				httpConnection.setDoInput(true);
				// This method implies that we intend to receive data from server.
				 httpConnection.setDoOutput(true);
				// Implies do not use cached data 
				 httpConnection.setUseCaches(false);
				// Data that will be sent over the stream to the server.
				DataOutputStream dataStreamToServer = new DataOutputStream(httpConnection.getOutputStream());
				dataStreamToServer.writeBytes("username=" + URLEncoder.encode(this.username, "UTF-8") + "&password="
						+ URLEncoder.encode(this.password, "UTF-8") + "&type=" + URLEncoder.encode(this.type, "UTF-8")
						+ "&dlr=" + URLEncoder.encode(this.dlr, "UTF-8") + "&destination="
						+ URLEncoder.encode(this.destination, "UTF-8") + "&source="
						+ URLEncoder.encode(this.source, "UTF-8") + "&message=" + URLEncoder.encode(this.message, "UTF-8"));
				dataStreamToServer.flush();
				dataStreamToServer.close(); 
				// Here take the output value of the server.
				BufferedReader dataStreamFromUrl = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
				String dataFromUrl = "", dataBuffer = "";
				// Writing information from the stream to the buffer
				while ((dataBuffer = dataStreamFromUrl.readLine()) != null) 
				{
					dataFromUrl += dataBuffer;
				}

				/**
				 * Now dataFromUrl variable contains the Response received from the server so we
				 * can parse the response and process it accordingly.
				 */
				dataStreamFromUrl.close();
				
				System.out.println("Response: " + dataFromUrl);
				
				mesage = dataFromUrl.substring(0, 4);
			} 
			catch (Exception ex) 
			{
				System.out.println("Response: " + ex.getMessage());
				System.out.println("error message - > "+ex.getMessage());
				ex.printStackTrace();
			}
			finally 
			{
				if (httpConnection != null) 
				{
					httpConnection.disconnect();
				}
			}
			return mesage;
		}
		
		
		
		private static void trustAllHttpsCertificates() throws Exception 
		{
			// Create a trust manager that does not validate certificate chains:
			javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
			javax.net.ssl.TrustManager tm = new miTM();

			trustAllCerts[0] = tm;
			javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, null);

			javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		}

		public static class miTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
				return true;
			}

			public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
				return true;
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
					throws java.security.cert.CertificateException {
				return;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
					throws java.security.cert.CertificateException {
				return;
			}
		}
		
		

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getDlr() {
			return dlr;
		}

		public void setDlr(String dlr) {
			this.dlr = dlr;
		}

		public String getDestination() {
			return destination;
		}

		public void setDestination(String destination) {
			this.destination = destination;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getServer() {
			return server;
		}

		public void setServer(String server) {
			this.server = server;
		}

		public int getPort() {
			return port;
		}

		public void setPort(int port) {
			this.port = port;
		}

}
