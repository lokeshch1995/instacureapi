package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "matched_profiles")
public class MatchedProfiles {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="matched_profile_id")
	private Long matchedProfileId;
	
	
	@OneToOne
	@JoinColumn(name = "post_new_job_id",referencedColumnName = "post_new_job_id")
	private PostNewJobDetails postNewJobDetails;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_seeker")
	private UserJobSeeker matchedJobSeekers;

	@Column(name = "profile_status")
	private String profileStatus;

	public Long getMatchedProfileId() {
		return matchedProfileId;
	}

	public void setMatchedProfileId(Long matchedProfileId) {
		this.matchedProfileId = matchedProfileId;
	}

	public PostNewJobDetails getPostNewJobDetails() {
		return postNewJobDetails;
	}

	public void setPostNewJobDetails(PostNewJobDetails postNewJobDetails) {
		this.postNewJobDetails = postNewJobDetails;
	}

	public String getProfileStatus() {
		return profileStatus;
	}

	public void setProfileStatus(String profileStatus) {
		this.profileStatus = profileStatus;
	}
	
	
}
