package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Employement_history")
public class EmploymentHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emph_id")
	private Long emphID;
	
	@OneToOne
	@JoinColumn(name = "js_id",referencedColumnName = "js_id")
	private UserJobSeeker jobSeeker;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id")
	private PostNewJobDetails previousJobs;
	
	
	@Column(name = "status")
    private String status;
	

    @Column(name="last_modified_date")
    private Date lastModifiedDate;

    @Column(name="last_modified_by")
    private String lastModifiedBy;

	public Long getEmphID() {
		return emphID;
	}

	public void setEmphID(Long emphID) {
		this.emphID = emphID;
	}

	public UserJobSeeker getJobSeeker() {
		return jobSeeker;
	}

	public void setJobSeeker(UserJobSeeker jobSeeker) {
		this.jobSeeker = jobSeeker;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public PostNewJobDetails getPreviousJobs() {
		return previousJobs;
	}

	public void setPreviousJobs(PostNewJobDetails previousJobs) {
		this.previousJobs = previousJobs;
	}

	

}
