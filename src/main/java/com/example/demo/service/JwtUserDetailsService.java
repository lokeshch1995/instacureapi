package com.example.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.dao.UserJobSeekerDao;
import com.example.demo.model.UserJobSeeker;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserJobSeekerDao jsDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println("in jwt userdetails");		
		System.out.println(username);
		UserJobSeeker userJs = jsDao.findByPhoneNumber(username);
		
		if(userJs == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		
		return new org.springframework.security.core.userdetails.User(userJs.getUserName(), userJs.getPassword(),
				new ArrayList<>());
	}

}
