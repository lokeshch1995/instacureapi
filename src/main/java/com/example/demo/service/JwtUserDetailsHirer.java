package com.example.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.dao.HirerDao;
import com.example.demo.model.Hirer;

@Service
public class JwtUserDetailsHirer implements UserDetailsService {
	
	@Autowired
	private HirerDao hirerDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println("in jwt userdetails");		
		System.out.println(username);
		Hirer hirer = hirerDao.findByPrimePocPhoneAndStatus(username, "ACTIVE");
		
		if(hirer == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		
		return new org.springframework.security.core.userdetails.User(hirer.getUserName(), hirer.getPassword(),
				new ArrayList<>());
	}

}
