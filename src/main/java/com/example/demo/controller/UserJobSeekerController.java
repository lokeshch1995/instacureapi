package com.example.demo.controller;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.JwtTokenUtil;
import com.example.demo.config.Status;
import com.example.demo.dao.PostNewJobDao;
import com.example.demo.dao.UserJobSeekerDao;
import com.example.demo.model.JwtResponse;
import com.example.demo.model.PostNewJobDetails;
import com.example.demo.model.UserJobSeeker;
import com.example.demo.service.JwtUserDetailsService;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/jobSeeker")
public class UserJobSeekerController {
	
	@Autowired
	private UserJobSeekerDao userJobSeekerDao;
	
	@Autowired 
	private PostNewJobDao postNewJobDao;
	
	@Autowired
	private PasswordEncoder bEncoder;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;
	
	
	@PostMapping(value ="/saveJobSeeker", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveJobseeker(@RequestBody UserJobSeeker userJobSeeker)
	{
		
		UserJobSeeker seeker_local = new UserJobSeeker();

		if(userJobSeeker.getOtp().isEmpty()) {
			
			  String otpNumber = String.valueOf(OTP(6));
			  userJobSeeker.setOtp(otpNumber);
			  userJobSeeker.setStatus(Status.INACTIVE);
			  userJobSeeker.setPassword(bEncoder.encode(userJobSeeker.getPassword()));
			  userJobSeekerDao.save(userJobSeeker);
			  
			  
			  System.out.println("otp " +otpNumber);
			  
			  System.out.println("if block");
		
		}
		else {
			 Date date = new Date(Calendar.getInstance().getTime().getTime());
			  
			  try {
				seeker_local =userJobSeekerDao.findByOtp(userJobSeeker.getOtp());
					
				  System.out.println("Otp from Db " +seeker_local.getOtp());
				  
				 System.out.println("----------------");
				  
				  
				    seeker_local.setStatus(Status.ACTIVE);
					seeker_local.setLastModifiedDate(date);
					seeker_local.setProfileSummary("hiiii");
					seeker_local.setUserName(seeker_local.getPhoneNumber());
				
				  userJobSeekerDao.save(seeker_local);
			} 
			  catch (NullPointerException e) {
				System.out.println("----------------");
				System.out.println("Otps are not matched ");
				System.out.println("----------------");
				e.printStackTrace();
			} 
		}
		 
			  
		 //String smsText  = " Get reigistered Successfully with OTP " +otpNumber+ "";
		
		 //OtpSender otpSender1 = new OtpSender("sms.digimiles.in", 8080, "di78-sbcktc",
			//	  "digimile", convertToUnicode(smsText).toString(), "1", "2",
				//  String.valueOf(userJobSeeker.getPhoneNumber()),"SBCKTC");
				  //otpSender1.submitMessage();
		
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(seeker_local.getPhoneNumber());
		final String token = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new JwtResponse(token));
		
	}
	
	
	@GetMapping(value = "/getJobSeekerByPhNo/{phoneNumber}")
	public UserJobSeeker findByPhoneNumber(@PathVariable String phoneNumber)
	{
		System.out.println("Into fetch query");
		System.out.println("Ph no " +phoneNumber);
		UserJobSeeker jobSeeker = userJobSeekerDao.findByPhoneNumber(phoneNumber);
		System.out.println("------------");
		System.out.println(jobSeeker.getPhoneNumber());
		System.out.println("-------------");
		System.out.println(jobSeeker.getEmail());
		
		System.out.println("Jobseeker status" +jobSeeker.getStatus());
		System.out.println(jobSeeker.getProfileSummary());
		System.out.println(jobSeeker.getType());
		System.out.println(jobSeeker.getCategory());
		return jobSeeker;
	}
	
	
	
	@GetMapping(value = "/allJobSeekers")
	public @ResponseBody List<UserJobSeeker> findAll()
	{
		System.out.println("in");
		List<UserJobSeeker> listOfJobSeekers = null;
		
		listOfJobSeekers = userJobSeekerDao.findAll();
		
		System.out.println(listOfJobSeekers.size());
		
		return listOfJobSeekers;
	}
	
	@GetMapping(value = "/getJobSeekerById/{id}")
	public Optional<UserJobSeeker> findById(@PathVariable Long id)
	{
		Optional<UserJobSeeker> jobSeeker = userJobSeekerDao.findById(id);
		return jobSeeker;
	}
	
	
	@PostMapping(value = "/updateJobseeker/{phoneNumber}")
	public void updateJobSeeker(@PathVariable String phoneNumber,@RequestBody UserJobSeeker userJobSeeker)
	{
		UserJobSeeker jobSeeker_local = userJobSeekerDao.findByPhoneNumber(phoneNumber);
		
		System.out.println(phoneNumber);
		
		System.out.println(jobSeeker_local.getName());
		
		System.out.println(userJobSeeker.getCategory());
		
		
		System.out.println(userJobSeeker.isSwitchValue());
		
		
		
		
		
		  
		  jobSeeker_local.setCategory(userJobSeeker.getCategory());
		  jobSeeker_local.setSpecilization(userJobSeeker.getSpecilization());
		  jobSeeker_local.setType(userJobSeeker.getType());
		  jobSeeker_local.setPreviousWorkPlace(userJobSeeker.getPreviousWorkPlace());
		  jobSeeker_local.setPlace(userJobSeeker.getPlace());
		  jobSeeker_local.setCountry(userJobSeeker.getCountry());
		  userJobSeekerDao.save(jobSeeker_local);
		 
	}
	
	@DeleteMapping(value = "/deleteJobSeekerById/{id}")
	public void deleteJobSeeker(@PathVariable Long id)
	{
		
		userJobSeekerDao.deleteById(id);
		
	}
	
	@GetMapping(value="/serachJobs")
	public List<PostNewJobDetails> searchJob(@RequestParam(required = false) String category,@RequestParam(required = false) String positionTitle,@RequestParam(required = false) String location)
	{
		System.out.println(category);
		System.out.println(positionTitle);
		System.out.println(location);
		
		List<PostNewJobDetails> listOfJobs = null;
		
		if(category != null && positionTitle != null && location!=null)
		{
			System.out.println("all values");
			listOfJobs =postNewJobDao.findByCategoryAndPositionTitleAndLocation(category, positionTitle, location);
		}
		
		else if (category !=null && positionTitle != null && location ==null)
		{
			System.out.println("category and position not null");
		    listOfJobs = postNewJobDao.findByCategoryAndPositionTitle(category, positionTitle);
		}
		
		else if(category !=null && positionTitle == null && location ==null)
		{
				System.out.println("cateegory only");
				 listOfJobs=postNewJobDao.findByCategory(category);
		}
		
		else if(category !=null && positionTitle == null && location !=null)
		{
			System.out.println("category and location");
			 listOfJobs = postNewJobDao.findByCategoryAndLocation(category, location);
		}
		
		else if(category ==null && positionTitle != null && location !=null)
		{
			System.out.println("position and location not null");
			 listOfJobs=postNewJobDao.findByPositionTitleAndLocation(positionTitle, location);
		}
		
		else if (category ==null && positionTitle != null && location ==null) {
			
			System.out.println("only position");
			 listOfJobs=postNewJobDao.findByPositionTitle(positionTitle);
		}
		
		else
		{
			System.out.println("only location");
			 listOfJobs=postNewJobDao.findByLocation(location);
		}
		
		
		
		return listOfJobs;
	}
	
	
	@SuppressWarnings("unused")
	private static StringBuffer convertToUnicode(String regText) 
	{
		char[] chars = regText.toCharArray();
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < chars.length; i++) 
		{
			String iniHexString = Integer.toHexString((int) chars[i]);
			if (iniHexString.length() == 1) 
			{
				iniHexString = "000" + iniHexString;
			} 
			else if (iniHexString.length() == 2) 
			{
				iniHexString = "00" + iniHexString;
			} 
			else if (iniHexString.length() == 3) 
			{
				iniHexString = "0" + iniHexString;
			}
			hexString.append(iniHexString);
		}
		System.out.println(hexString);
		return hexString;
	}
	static char[] OTP(int len) {
		System.out.println("Generating OTP using random ()");
		System.out.print("Your OTP is:");

		// Using numeric values
		String numbers = "0123456789";

		// Using random method
		Random rndm_method = new Random();
		char[] otp = new char[len];

		for (int i = 0; i < len; i++) {
			// use of charAt() method : to get character value
			// use of nextInt() as it is scanning the value as int
			otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return otp;
	}

}
