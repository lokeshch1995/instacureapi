package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.JsNotificationsDao;
import com.example.demo.model.JsNotification;

@RestController
@RequestMapping(value = "/jsNotifications")
public class JsNotificationsController {
	
	@Autowired
	private JsNotificationsDao jsNotificationsDao;

	
	@PostMapping(value ="/saveJsNotification")
	public void saveJsNotification(@RequestBody JsNotification jsNotification)
	{
		jsNotificationsDao.save(jsNotification);
	}
	
	@GetMapping(value = "/allJsNotification")
	public @ResponseBody List<JsNotification> findAll()
	{
		List<JsNotification> jsNotification = null;
		
		jsNotification = jsNotificationsDao.findAll();
		
		return jsNotification;
	}
	
	@GetMapping(value = "/getJsNotificationId/{id}")
	public Optional<JsNotification> findById(@PathVariable Long id)
	{
		Optional<JsNotification> jsNotification = jsNotificationsDao.findById(id);
		return jsNotification;
	}
	
	
	@PutMapping(value = "/updateJsNotification/{id}")
	public void updateJsNotification(@PathVariable Long id,@RequestBody JsNotification jsNotification)
	{
		JsNotification jsNotification_local = jsNotificationsDao.findByNotfId(id);
		
		jsNotification_local.setNotfId(id);
		
		jsNotification_local.setJobNotification(jsNotification.getJobNotification());
		jsNotification_local.setStatus(jsNotification.getStatus());
		
		jsNotificationsDao.save(jsNotification_local);
	}
	
	@DeleteMapping(value = "/deleteJsNotificationById/{id}")
	public void deleteJsNotification(@PathVariable Long id)
	{
		
		jsNotificationsDao.deleteById(id);
	}
	
}
