package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.CategoryDao;
import com.example.demo.model.Category;

@RestController
@RequestMapping(value = "/categories")
public class CategoryController {
	
	@Autowired
	private CategoryDao categoryDao;
	
	@GetMapping(value = "/getAllCategories")
	public List<Category> findAllCategories(){
		
		List<Category> categories = categoryDao.findAll();
		
		return categories;
	}

}
