package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.JsSavedJobsDao;
import com.example.demo.model.JsSavedJobs;

@RestController
@RequestMapping(value = "/jsSavedJobs")
public class JsSavedJobsController {
	
	@Autowired
	private JsSavedJobsDao jsSavedJobsDao;
	
	
	@PostMapping(value ="/saveJsSavedJobs")
	public void saveJsSavedJobs(@RequestBody JsSavedJobs jsSavedJobs)
	{
		jsSavedJobsDao.save(jsSavedJobs);
	}
	
	@GetMapping(value = "/allJsSavedJobs")
	public @ResponseBody List<JsSavedJobs> findAll()
	{
		List<JsSavedJobs> jsSavedJobs = null;
		
		jsSavedJobs = jsSavedJobsDao.findAll();
		
		return jsSavedJobs;
	}
	
	@GetMapping(value = "/getJsSavedJobsById/{id}")
	public Optional<JsSavedJobs> findById(@PathVariable Long id)
	{
		Optional<JsSavedJobs> jsSavedJobs = jsSavedJobsDao.findById(id);
		return jsSavedJobs;
	}
	
	
	@PutMapping(value = "/updateJsSavedJobs/{id}")
	public void updateJobSeeker(@PathVariable Long id,@RequestBody JsSavedJobs jsSavedJobs)
	{
		JsSavedJobs jsSavedJobs_local = jsSavedJobsDao.findByJsSavedJobsId(id);
		
		jsSavedJobs_local.setJsSavedJobsId(id);
		
		jsSavedJobs_local.setJobsSaved(jsSavedJobs.getJobsSaved());
		jsSavedJobs_local.setUserJobSeeker(jsSavedJobs.getUserJobSeeker());
		jsSavedJobs_local.setStatus(jsSavedJobs.getStatus());
		
		jsSavedJobsDao.save(jsSavedJobs_local);
	}
	
	@DeleteMapping(value = "/deleteJsSavedJobsById/{id}")
	public void deleteJsSavedJobs(@PathVariable Long id)
	{
		
		jsSavedJobsDao.deleteById(id);
	}

}
