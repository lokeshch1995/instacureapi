package com.example.demo.controller;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.constants.H20Constants;
import com.example.demo.dao.PostNewJobDao;
import com.example.demo.model.PostNewJobDetails;

@RestController
@RequestMapping(value="/postJob")
public class PostNewJobController {

	@Autowired
	private PostNewJobDao postNewJobDao;
	
	@PostMapping(value="/savePostJob")
	public void addPostJob(@RequestBody PostNewJobDetails postNewJobDetails) {
		System.out.println(postNewJobDetails);
		postNewJobDetails.setStatus(H20Constants.ACTIVE);
		
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		
		postNewJobDetails.setLastModifiedDate(date);
		
		postNewJobDao.save(postNewJobDetails);
	}
	
	@GetMapping(value="/getAllNewPostedJobs")
	public List<PostNewJobDetails> getAllNewJobs() {
		
		List<PostNewJobDetails> newJobDetails =null;
		newJobDetails=postNewJobDao.findAll();
		
		return newJobDetails;
		
	}
	
	@GetMapping(value="/getAllNewPostedJobsByID/{postNewJobId}")
	public Optional<PostNewJobDetails> findById(@PathVariable Long postNewJobId) {
	
		Optional<PostNewJobDetails> newJobDetails =postNewJobDao.findById(postNewJobId);
		return newJobDetails;
		
	}
	
	@PostMapping(value="/updatePostNewJobDetails/{postNewJobId}")
	public void updateNewJobs(@PathVariable Long postNewJobId,@RequestBody PostNewJobDetails postNewJobDetails) {
		
		PostNewJobDetails details = postNewJobDao.findByPostNewJobId(postNewJobId);
		
		details.setPositionTitle(postNewJobDetails.getPositionTitle());
		details.setAssgStartDate(postNewJobDetails.getAssgStartDate());
		details.setAssgPlannedEnddate(postNewJobDetails.getAssgPlannedEnddate());
		details.setLocation(postNewJobDetails.getLocation());
		details.setType(postNewJobDetails.getType());
		details.setSalaryDetails(postNewJobDetails.getSalaryDetails());
		details.setLastModifiedBy(postNewJobDetails.getLastModifiedBy());
		
		
		details.setStatus(H20Constants.ACTIVE);
		
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		
		details.setLastModifiedDate(date);
		
		postNewJobDao.save(details);
		
	}
	
	@DeleteMapping(value="/deleteNewJobDetails/{postNewJobId}")
	public void deleteNewJobDetails(@PathVariable Long postNewJobId) {
		
		postNewJobDao.deleteById(postNewJobId);
	}
	
}
