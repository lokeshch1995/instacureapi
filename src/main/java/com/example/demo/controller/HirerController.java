package com.example.demo.controller;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.constants.H20Constants;
import com.example.demo.dao.HirerDao;
import com.example.demo.model.Hirer;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/hirer")
public class HirerController {

	 @Autowired 
	 private HirerDao hirerDao;
	 
	 @Autowired
	 private PasswordEncoder bEncoder;

	@PostMapping(value="/saveHirer")
	public void saveHirerDetails(@RequestBody Hirer userHirer) {
	
		System.out.println(userHirer.getNameOfOrganization());
		System.out.println(userHirer.getSector());
		userHirer.setStatus("INACTIVE");
		userHirer.setPassword(bEncoder.encode(userHirer.getPassword()));
		userHirer.setUserName(userHirer.getPrimePocPhone());
		
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		
		userHirer.setLastModifiedDate(date);
		
		hirerDao.save(userHirer);
	}
	
	@GetMapping(value="/getAllHirers")	
	public @ResponseBody List<Hirer> getAllHirers() {
		List<Hirer> list =null;
		list=hirerDao.findAll();
		
		return list;	
	}
	
	@GetMapping(value="/getHirersById/{hrId}")
	public Optional<Hirer> getHirersByID(@PathVariable Long hrId) {
		
		Optional<Hirer> userHirer = hirerDao.findById(hrId);
		
		return userHirer;
	}
	

	@PostMapping("/updateHirers/{hrId}")
	public void updateHirer(@PathVariable Long hrId,@RequestBody Hirer hirer) throws Exception
	{
		System.out.println("ID " +hrId);
		Hirer hirer1=hirerDao.findByHrId(hrId);
		
		hirer1.setBranches(hirer.getBranches());
		hirer1.setBeds(hirer.getBeds());
		hirer1.setDepartments(hirer.getDepartments());
		hirer1.setHeadOfficeAddr(hirer.getHeadOfficeAddr());
		hirer1.setLastModifiedBy(hirer.getLastModifiedBy());
		hirer1.setNameOfOrganization(hirer.getNameOfOrganization());
		hirer1.setpPocDesignation(hirer.getpPocDesignation());
		hirer1.setpPocOfficeEmail(hirer.getpPocOfficeEmail());
		hirer1.setPrimePocPhone(hirer.getPrimePocPhone());
		hirer1.setPrimePocName(hirer.getPrimePocName());
		
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		
        hirer1.setLastModifiedDate(date);
		hirer1.setStatus("Active");
		
		
		hirerDao.save(hirer1);
	
	}
	
	@DeleteMapping(value="/deleteHirersById/{hrId}")
	public void deleteHirers(@PathVariable Long hrId) {
		
		hirerDao.deleteById(hrId);
	}
	
	@GetMapping(value = "/getHirerByPocPhone/{number}")
	public Hirer getHirerDetailsByPocNumber(@PathVariable String number) {
		
		Hirer hirer = hirerDao.findByPrimePocPhoneAndStatus(number, H20Constants.ACTIVE);
		
		return hirer;
	}
	
}
