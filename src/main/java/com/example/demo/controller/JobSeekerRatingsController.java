package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.JobSeekerRatingsDao;
import com.example.demo.model.JobSeekerRatings;

@RestController
@RequestMapping(value = "/jobSekkerRatings")
public class JobSeekerRatingsController {
	
	@Autowired
	private JobSeekerRatingsDao jobSeekerRatingsDao;
	
	
	@PostMapping(value ="/saveJobSeekerRatings")
	public void saveJobSeekerRatings(@RequestBody JobSeekerRatings jobSeekerRatings)
	{
		jobSeekerRatingsDao.save(jobSeekerRatings);
	}
	
	@GetMapping(value = "/allJobSeekerRatings")
	public @ResponseBody List<JobSeekerRatings> findAll()
	{
		List<JobSeekerRatings> jobSeekerRatings = null;
		
		jobSeekerRatings = jobSeekerRatingsDao.findAll();
		
		return jobSeekerRatings;
	}
	
	@GetMapping(value = "/getJobSeekerRatingsById/{id}")
	public Optional<JobSeekerRatings> findById(@PathVariable Long id)
	{
		Optional<JobSeekerRatings> jobSeekerRatings = jobSeekerRatingsDao.findById(id);
		return jobSeekerRatings;
	}
	
	
	@PutMapping(value = "/updateJobSeekerRatings/{id}")
	public void updateJobSeekerRatings(@PathVariable Long id,@RequestBody JobSeekerRatings jobSeekerRatings)
	{
		JobSeekerRatings jobSeekerRatings_local = jobSeekerRatingsDao.findByJsRatingId(id);
		
		jobSeekerRatings_local.setJsRatingId(id);
		
		jobSeekerRatings_local.setStarCount(jobSeekerRatings.getStarCount());
		jobSeekerRatings_local.setStatus(jobSeekerRatings.getStatus());
		jobSeekerRatings_local.setHirer(jobSeekerRatings.getHirer());
		
		jobSeekerRatingsDao.save(jobSeekerRatings_local);
	}
	
	@DeleteMapping(value = "/deleteJobSeekerRatingsById/{id}")
	public void deleteJobSeekerRatings(@PathVariable Long id)
	{
		
		jobSeekerRatingsDao.deleteById(id);
	}

}
