package com.example.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.AuthenticationBean;

@CrossOrigin(origins = "*")
@RestController
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public AuthenticationBean login() {
		System.out.println("login");
		System.out.println("after login");
		return new AuthenticationBean("You are authenticated");
	}

}
