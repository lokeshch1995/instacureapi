package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.PointOfContactDao;
import com.example.demo.model.PointOfContact;

@RestController
@RequestMapping(value = "/pointOfContact")
public class PointOfContactController {
	
	@Autowired
	private PointOfContactDao pointOfContactDao;
	
	@PostMapping(value ="/savePointOfContact")
	public void savePointOfContact(@RequestBody PointOfContact pointOfContact)
	{
		pointOfContactDao.save(pointOfContact);
	}
	
	@GetMapping(value = "/allPointOfContact")
	public @ResponseBody List<PointOfContact> findAll()
	{
		List<PointOfContact> pointOfContacts = null;
		
		pointOfContacts = pointOfContactDao.findAll();
		
		return pointOfContacts;
	}
	
	@GetMapping(value = "/getPointOfContactById/{id}")
	public Optional<PointOfContact> findById(@PathVariable Long id)
	{
		Optional<PointOfContact> pointOfContact = pointOfContactDao.findById(id);
		return pointOfContact;
	}
	
	@PutMapping(value = "/updatePointOfContact/{id}")
	public void updatePointOfContact(@PathVariable Long id,@RequestBody PointOfContact pointOfContact)
	{
		PointOfContact pointOfContact_local = pointOfContactDao.findByPocId(id);
		
		
		
		pointOfContact_local.setPocId(id);
		
		pointOfContact_local.setName(pointOfContact.getName());
		pointOfContact_local.setDesignation(pointOfContact.getDesignation());
		pointOfContact_local.setPhoneNumber(pointOfContact.getPhoneNumber());
		pointOfContact_local.setOfficeEmail(pointOfContact.getOfficeEmail());
		pointOfContact_local.setStatus(pointOfContact.getStatus());
		
		pointOfContactDao.save(pointOfContact_local);
	}
	
	@DeleteMapping(value = "/deletePointOfContactById/{id}")
	public void deletePointOfContact(@PathVariable Long id)
	{
		
		pointOfContactDao.deleteById(id);
		
	}

}
