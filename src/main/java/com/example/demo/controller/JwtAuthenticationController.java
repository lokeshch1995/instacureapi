package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.JwtTokenUtil;
import com.example.demo.model.JwtRequest;
import com.example.demo.model.JwtResponse;
import com.example.demo.service.JwtUserDetailsHirer;
import com.example.demo.service.JwtUserDetailsService;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;
	
	@Autowired
	private JwtUserDetailsHirer hirerService;
	
	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception
	{
		System.out.println(jwtRequest.getUsername());
		System.out.println(jwtRequest.getPassword());
		authenticate(jwtRequest.getUsername(), jwtRequest.getPassword());
		
		final UserDetails userDetails= jwtUserDetailsService.loadUserByUsername(jwtRequest.getUsername());
		
		final String token = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	@PostMapping(value = "/authenticateHirer")
	public ResponseEntity<?> createAuthenticationTokenHirer(@RequestBody JwtRequest jwtRequest) throws Exception
	{
		System.out.println(jwtRequest.getUsername());
		System.out.println(jwtRequest.getPassword());
		authenticate(jwtRequest.getUsername(), jwtRequest.getPassword());
		
		final UserDetails userDetails= hirerService.loadUserByUsername(jwtRequest.getUsername());
		
		final String token = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	private void authenticate(String username,String password) throws Exception
	{
		System.out.println("in authenticate");
		try {
			System.out.println("in try");
			
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			
		} catch (DisabledException e) {
			
			throw new Exception("USER_DISABLED",e);
		}
		
		catch (BadCredentialsException e) {
			
			throw new Exception("BAD_CREDENTIALS",e);
		}
		
	}

}
