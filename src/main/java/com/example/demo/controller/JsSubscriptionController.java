package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.JsSubscriptionDao;
import com.example.demo.model.JsSubscription;

@RestController
@RequestMapping(value = "/jsSubscription")
public class JsSubscriptionController {
	
	@Autowired
	private JsSubscriptionDao jsSubscriptionDao;
	
	@PostMapping(value ="/saveJsSubscription")
	public void saveJsSubscription(@RequestBody JsSubscription jsSubscription)
	{
		jsSubscriptionDao.save(jsSubscription);
	}
	
	@GetMapping(value = "/allJsSubscriptions")
	public @ResponseBody List<JsSubscription> findAll()
	{
		List<JsSubscription> jsSubscriptions = null;
		
		jsSubscriptions = jsSubscriptionDao.findAll();
		
		return jsSubscriptions;
	}
	
	@GetMapping(value = "/getJsSubscriptionById/{id}")
	public Optional<JsSubscription> findById(@PathVariable Long id)
	{
		Optional<JsSubscription> jsSubscription = jsSubscriptionDao.findById(id);
		return jsSubscription;
	}
	
	
	@PutMapping(value = "/updateJsSubscription/{id}")
	public void updateJobSeeker(@PathVariable Long id,@RequestBody JsSubscription jsSubscription)
	{
		JsSubscription jsSubscription_local = jsSubscriptionDao.findBySubId(id);
		
		jsSubscription_local.setSubId(id);
		
		jsSubscription_local.setSubName(jsSubscription.getSubName());
		jsSubscription_local.setPrice(jsSubscription.getPrice());
		jsSubscription_local.setStatus(jsSubscription.getStatus());
		jsSubscription_local.setDuration(jsSubscription.getDuration());
		
		jsSubscriptionDao.save(jsSubscription_local);
	}
	
	@DeleteMapping(value = "/deleteJsSubscriptionById/{id}")
	public void deleteJsSubscription(@PathVariable Long id)
	{
		
		jsSubscriptionDao.deleteById(id);
		
	}

}
