package com.example.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(origins = "*")
@RestController
public class HomeController {

	
	@GetMapping(value="/homePage")
	public void home() {
		
		System.out.println("logged in");
		
	}
}
