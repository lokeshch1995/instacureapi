package com.example.demo.controller;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.constants.H20Constants;
import com.example.demo.dao.EmployerRatingDao;
import com.example.demo.model.EmployerRating;

@RestController
@RequestMapping(value="/employeeRating")
public class EmployementRatingController {

	@Autowired
	private EmployerRatingDao employerRatingDao;
	
	@PostMapping(value="/saveEmployerRatings")
	public void saveEmployementRating(@RequestBody EmployerRating employerRating) {
		
		employerRating.setStatus(H20Constants.ACTIVE);
		
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		employerRating.setLastModifiedDate(date);
		employerRatingDao.save(employerRating);
	}
	
	@GetMapping(value="/getEmployeeRatings")
	public List<EmployerRating> getAllEmployeeRatings() {
		
		List<EmployerRating> ratings = employerRatingDao.findAll();
		return ratings;
		
	}
	
	@GetMapping(value="/findRatingsById/{jsRatingId}")
	public Optional<EmployerRating> getRatingsByID(@PathVariable Long jsRatingId) {
		Optional<EmployerRating> employerRating = employerRatingDao.findById(jsRatingId);
		
		return employerRating;
		
	}
	
	@PostMapping(value="/updateEmployeeRatings/{jsRatingId}")
	public void updateEmployeeRatings(@PathVariable Long jsRatingId,EmployerRating employerRating) {
		
		EmployerRating rating = employerRatingDao.findByEmployerRatingId(jsRatingId);
		
		rating.setCommunication(employerRating.getCommunication());
		rating.setExperience(employerRating.getExperience());
		rating.setInteractionWithEmp(employerRating.getInteractionWithEmp());
		rating.setTimeManagement(employerRating.getTimeManagement());
		rating.setLastModifiedBy(employerRating.getLastModifiedBy());
		rating.setStatus(H20Constants.ACTIVE);
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		rating.setLastModifiedDate(date);
		
	}
	
	@DeleteMapping(value="/deleteRatings/{jsRatingId}")
	public void deleteEmpRatings(@PathVariable Long jsRatingId) {
		employerRatingDao.deleteById(jsRatingId);
	}
}
