package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.SpecilizationDao;
import com.example.demo.model.Specilization;

@RestController
@RequestMapping(value = "/specilization")
public class SpecilizationController {
	
	@Autowired
	private SpecilizationDao specilizationDao;
	
	@GetMapping(value = "/getSpeclizationByCategory/{id}")
	public List<Specilization> findByCategory(@PathVariable Long id){
		
		List<Specilization> speclizations = specilizationDao.findByCategoryId(id);
		
		return speclizations;
	}

}
