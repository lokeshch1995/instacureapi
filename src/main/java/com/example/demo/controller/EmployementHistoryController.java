package com.example.demo.controller;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.constants.H20Constants;
import com.example.demo.dao.EmployementHistoryDao;
import com.example.demo.model.EmploymentHistory;

@RestController
@RequestMapping(value="/employerHistory")
public class EmployementHistoryController {

	
	@Autowired
	private EmployementHistoryDao employementHistoryDao;
	
	@PostMapping(value="/saveEmployement")
	public void saveEmplooyementHistory(@RequestBody EmploymentHistory  employmentHistory) {
		
		
		employmentHistory.setStatus(H20Constants.ACTIVE);
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		employmentHistory.setLastModifiedDate(date);
		employementHistoryDao.save(employmentHistory);
		
		
		
	}
	
	@GetMapping(value="/getAllEmpHistory")
	public List<EmploymentHistory> getAllTheHistory() {
		
		List<EmploymentHistory> employmentHistories = employementHistoryDao.findAll();
		return employmentHistories;
		
	}
	@GetMapping(value="/getHistoryByID/{emphID}")
	public Optional<EmploymentHistory> findEmpHistoryById(@PathVariable Long emphID) {
		Optional<EmploymentHistory> history = employementHistoryDao.findById(emphID);
		
		return history;
		
	}
	
	@PostMapping(value="/updateEmploementHistory/{emphID}")
	public void updateEmpHsitory(@PathVariable Long emphID,@RequestBody EmploymentHistory employmentHistory) {
		
		EmploymentHistory employmentHistory1 =  employementHistoryDao.findByEmphID(emphID);
		
		employmentHistory1.setPreviousJobs(employmentHistory.getPreviousJobs());
		employmentHistory1.setJobSeeker(employmentHistory.getJobSeeker());
		
		employmentHistory1.setLastModifiedBy(employmentHistory.getLastModifiedBy());
		
		Date date = new Date(Calendar.getInstance().getTime().getTime());
		employmentHistory1.setLastModifiedDate(date);
		
		employmentHistory1.setStatus(H20Constants.ACTIVE);
		
		employementHistoryDao.save(employmentHistory1);
		
	}
	
	@DeleteMapping(value="/deleteHistoryById/{emphID}")
	public void deleteEmpHistory(@PathVariable Long emphID) {
		employementHistoryDao.deleteById(emphID);
	}
	
}
