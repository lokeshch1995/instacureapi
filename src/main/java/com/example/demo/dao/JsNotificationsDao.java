package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.JsNotification;

public interface JsNotificationsDao extends JpaRepository<JsNotification, Long>{
	
	
	JsNotification findByNotfId(Long notfId);

}
