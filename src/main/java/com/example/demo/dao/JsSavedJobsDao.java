package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.JsSavedJobs;

public interface JsSavedJobsDao extends JpaRepository<JsSavedJobs, Long> {
	
	JsSavedJobs findByJsSavedJobsId(Long jsSavedJobsId);

}
