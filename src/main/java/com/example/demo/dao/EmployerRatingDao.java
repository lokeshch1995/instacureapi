package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.EmployerRating;

public interface EmployerRatingDao extends JpaRepository<EmployerRating, Long> {

	EmployerRating findByEmployerRatingId(Long jsRatingId);
}
