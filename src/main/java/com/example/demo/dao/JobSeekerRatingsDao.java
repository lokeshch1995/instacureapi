package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.JobSeekerRatings;

public interface JobSeekerRatingsDao extends JpaRepository<JobSeekerRatings, Long>{
	
	
	JobSeekerRatings findByJsRatingId(Long jsRatingId);

}
