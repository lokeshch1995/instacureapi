package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.PointOfContact;

public interface PointOfContactDao extends JpaRepository<PointOfContact, Long>{
	
	PointOfContact findByPocId(Long pocId);

}
