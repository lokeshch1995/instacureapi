package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.EmploymentHistory;

public interface EmployementHistoryDao extends JpaRepository<EmploymentHistory, Long>{

	EmploymentHistory findByEmphID(Long emphID);
}
