package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Specilization;

public interface SpecilizationDao extends JpaRepository<Specilization, Long> {
	
	List<Specilization> findByCategoryId(Long id);

}
