package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.UserJobSeeker;

public interface UserJobSeekerDao extends JpaRepository<UserJobSeeker, Long>
{
	
	UserJobSeeker findByJsId(Long id);
	
	UserJobSeeker findByOtp(String otp);
	
	UserJobSeeker findByPhoneNumber(String phoneNumber);
}