package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.PostNewJobDetails;

public interface PostNewJobDao extends JpaRepository<PostNewJobDetails, Long>{

	PostNewJobDetails findByPostNewJobId(Long postNewJobId);
	
	List<PostNewJobDetails> findByCategoryAndPositionTitleAndLocation(String category,String positionTitle,String location);
	
	List<PostNewJobDetails> findByCategory(String category);
	
	List<PostNewJobDetails> findByPositionTitle(String positionTitle);
	
	List<PostNewJobDetails> findByLocation(String location);
	
	List<PostNewJobDetails> findByCategoryAndPositionTitle(String category,String positionTitle);
	
	List<PostNewJobDetails> findByCategoryAndLocation(String category,String location);
	
	List<PostNewJobDetails> findByPositionTitleAndLocation(String positionTitle,String location);
	
}
