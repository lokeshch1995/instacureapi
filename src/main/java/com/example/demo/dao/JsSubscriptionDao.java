package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.JsSubscription;

public interface JsSubscriptionDao extends JpaRepository<JsSubscription, Long>{
	
	
	JsSubscription findBySubId(Long subId);
}
