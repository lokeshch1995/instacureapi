package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Hirer;

public interface HirerDao extends JpaRepository<Hirer, Long>{

Hirer findByHrId(Long hrId);

Hirer findByPrimePocPhoneAndStatus(String number, String status);

}
